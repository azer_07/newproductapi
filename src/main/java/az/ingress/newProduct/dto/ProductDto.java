package az.ingress.newProduct.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

    private Long id;
    private String name;
    private Double price;
    private Long count;
    private ManufacturerDto manufacturerDto;
    private Set<CategoryDto> categoryDto;

}
