package az.ingress.newProduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewProductApplication.class, args);
	}

}
