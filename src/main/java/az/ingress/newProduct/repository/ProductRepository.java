package az.ingress.newProduct.repository;

import az.ingress.newProduct.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Set<Product> findAllByPriceGreaterThan(Double price);
    Set<Product> findAllByCategoriesNameEquals(String category);

}
