package az.ingress.newProduct.service;

import az.ingress.newProduct.dto.CategoryDto;
import az.ingress.newProduct.dto.ManufacturerDto;
import az.ingress.newProduct.dto.ProductDto;
import az.ingress.newProduct.model.Category;
import az.ingress.newProduct.model.Manufacturer;
import az.ingress.newProduct.model.Product;
import az.ingress.newProduct.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
   private final ProductRepository productRepository;
    private final ModelMapper mapper=new ModelMapper();



    @Override
    public Set<ProductDto> getAllProducts() {
        Set<ProductDto> list = productRepository.findAll().stream().map(product -> {
            ProductDto productDto = mapper.map(product, ProductDto.class);
            return productDto;

        }).collect(Collectors.toSet());
        return list;
    }

    @Override
    public ProductDto save(ProductDto dto) {
        Manufacturer manufacturer = mapper.map(dto.getManufacturerDto(), Manufacturer.class);
        Product product = mapper.map(dto, Product.class);
        product.setManufacturer(manufacturer);

       Set<Category> categories= dto.getCategoryDto().stream().map(categoryDto -> {
           Category category=mapper.map(categoryDto,Category.class);
           return category;
           }).collect(Collectors.toSet());
         product.setCategories(categories);
        productRepository.save(product);
        return dto;
    }

    @Override
    public Set<ProductDto> getProductWithPrice(Double price) {

        Set<Product> productSet= productRepository.findAllByPriceGreaterThan(price);
        Set<ProductDto> list = productSet.stream().map(product -> {
            ProductDto productDto = mapper.map(product, ProductDto.class);
        ManufacturerDto manufacturerDto=mapper.map(product.getManufacturer(), ManufacturerDto.class);
          productDto.setManufacturerDto(manufacturerDto);
            Set<CategoryDto>categoryDtos=product.getCategories().stream().map(category -> {
                CategoryDto categoryDto=mapper.map(category,CategoryDto.class);
                return categoryDto;
            }).collect(Collectors.toSet());
            productDto.setCategoryDto(categoryDtos);

          return productDto;
        }).collect(Collectors.toSet());

        return list;
    }

    @Override
    public Set<ProductDto> getProductWithCategory(String category) {
        Set<Product> products=productRepository.findAllByCategoriesNameEquals(category);
        Set<ProductDto> list = products.stream().map(product -> {
            ProductDto productDto = mapper.map(product, ProductDto.class);
            ManufacturerDto manufacturerDto=mapper.map(product.getManufacturer(),ManufacturerDto.class);
            productDto.setManufacturerDto(manufacturerDto);
            Set<CategoryDto>categoryDtos=product.getCategories().stream().map(category1 -> {
                CategoryDto categoryDto=mapper.map(category1,CategoryDto.class);
                return categoryDto;
            }).collect(Collectors.toSet());
            productDto.setCategoryDto(categoryDtos);
            return productDto;
        }).collect(Collectors.toSet());
        return list;
    }
}
