package az.ingress.newProduct.service;

import az.ingress.newProduct.dto.ProductDto;
import az.ingress.newProduct.repository.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;


public interface ProductService {


    Set<ProductDto> getAllProducts();

    ProductDto save(ProductDto dto);

    Set<ProductDto> getProductWithPrice(Double price);

    Set<ProductDto> getProductWithCategory(String category);
}
