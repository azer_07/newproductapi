package az.ingress.newProduct.controller;

import az.ingress.newProduct.dto.CategoryDto;
import az.ingress.newProduct.dto.ProductDto;
import az.ingress.newProduct.service.ProductService;
import az.ingress.newProduct.service.ProductServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")

public class ProductController {
    private final ProductService productService;

    @GetMapping()
    public Set<ProductDto> getAllProducts(){
      return productService.getAllProducts();
    }

    @PostMapping
    public ProductDto save(@RequestBody ProductDto dto){
        return productService.save(dto);
    }

    @GetMapping("/price")
    public Set<ProductDto> getProductWithPrice(@RequestParam Double price){
        return productService.getProductWithPrice(price);
    }
    @GetMapping("/category")
    public Set<ProductDto> getProductWithCategory(@RequestParam String category){
        return productService.getProductWithCategory(category);
    }
}
